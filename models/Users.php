<?php
namespace MyApp\Models\Users;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Users extends Model
{
    public $id;
    public $name;
    
    public function validation()
    {
        $validator = new Validation();
        $validator->add(
            'userName',
            new Uniqueness(
                [
                    'message' => 'The username already taken',
                ]
            )
        );
        return $this->validate($validator);
    }
     public function initialize()
     {
         $this->hasMany('id' , 'Services' , 'id');
     }
}