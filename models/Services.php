<?php
namespace MyApp\Models\Services;

use Phalcon\Mvc\Model;

class Services extends Model
{
    public $id;
    public $name;
    
    public function initialize()
    {
        $this->hasMany('id' , 'Users' , 'id');
    }
}