<?php
use Phalcon\Mvc\Micro\Collection as MicroCollection;

return call_user_func(function(){
    $test = new MicroCollection();
    $test->setHandler(new \MyApp\Api\TestsController\Controller());
    $test->setPrefix('/tests');
    $test->get('/', 'getTests');
    $test->get('/result', 'testResult');
    $test->post('/', 'createTests');
    $test->post('/answers', 'answerTests');
    return $test;
});