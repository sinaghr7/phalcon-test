<?php

namespace MyApp\Api\TestsController;

use MyApp\Models\Test\Test;
use Phalcon\Di\Injectable;
use MyApp\Models\Answer\Answer;

class Controller extends Injectable
{
    public function getTests()
    {
        $test = Test::findFirst();
        $test->questions = json_decode($test->questions);
        for ($i = 0; $i < 60; $i++) {
            $help[$i]["سوال"] = $test->questions[$i]->{'سوال'};
            $help[$i]["متن"] = $test->questions[$i]->{'متن'};
            $help[$i]["الف"] = $test->questions[$i]->{'الف'}->{'0'};
            $help[$i]["ب"] = $test->questions[$i]->{'ب'}->{'0'};
        }
        if (!$test) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find test!");
            return $this->response;
        } else {
            $this->response->setJsonContent(
                $help
            );
            return $this->response;
        }
    }

    public function createTests()
    {
        $dir = dirname(__FILE__);
        $lines = file($dir . '/test.txt');
        $string = str_replace(array("\r", "\n", "\t"), '', $lines);
        foreach ($string as $line_num) {
            $test[] = $line_num;
            $lastChar[] = substr($line_num, -1);
        }
        for ($i = 0; $i < 120; $i++) {
            $test[$i] = str_replace($lastChar[$i], '', $string[$i]);
        }
        for ($x = 0, $z = 1; $x < 120; $x = $x + 2, $z++) {
            $y = $x + 1;
            $array["سوال"] = $z;
            $array["متن"] = "کدام گزینه را انتخاب میکنید ؟";
            $help1[] = $test[$x];
            $help2[] = $test[$y];
            $array ["الف"] = $help1;
            $array ["الف"]["محور"] = $lastChar[$x];
            $array ["ب"] = $help2;
            $array ["ب"]["محور"] = $lastChar[$x + 1];
            $result[] = $array;
            unset($array, $help1, $help2);
        }
        $example = json_encode($result);
        $test = new Test();
        $test->questions = $example;
        if ($test->create() === false) {
            $this->response->setStatusCode(404);
            $this->response->setContent("we cant create test");
            return $this->response;
        } else {
            return $this->getTests();
        }
    }

    public function answerTests()
    {
        $test = Test::findFirst();
        $test->questions = json_decode($test->questions);
        $data = $this->request->getJsonRawBody(true);
        $answer = Answer::findFirst([
            "conditions" => "test_id = :test_id: AND user_id = :user_id:",
            "bind" => [
                "test_id" => $data["testId"],
                "user_id" => $this->di->get("userObject")->id ,
            ]
        ]);
        if ($answer == true) {
            return $this->updateAnswer();
        } else {
            $answer = new Answer();
            $answer->user_id = $this->di->get("userObject")->id;
            $answer->test_id = $data["testId"];
            if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "e") {
                $answer->e = $answer->e + 1;
            } else {
                $answer->i = $answer->i + 1;
            }
            if ($answer->create() === false) {
                $this->response->setStatusCode(406);
                $this->response->setContent("we cant create answer");
                return $this->response;
            } else {
                $this->response->setContent("go on");
                return $this->response;
            }
        }
    }

    public function updateAnswer()
    {
        $test = Test::findFirst();
        $test->questions = json_decode($test->questions);
        $data = $this->request->getJsonRawBody(true);
        $answer = Answer::findFirst([
            "conditions" => "test_id = :test_id: AND user_id = :user_id:",
            "bind" => [
                "test_id" => $data["testId"],
                "user_id" => $this->di->get("userObject")->id ,
            ]
        ]);
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "e") {
            $answer->e = $answer->e + 1;
        }
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "f") {
            $answer->f = $answer->f + 1;
        }
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "i") {
            $answer->i = $answer->i + 1;
        }
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "j") {
            $answer->j = $answer->j + 1;
        }
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "n") {
            $answer->n = $answer->n + 1;
        }
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "p") {
            $answer->p = $answer->p + 1;
        }
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "s") {
            $answer->s = $answer->s + 1;
        }
        if ($test->questions[$data["سوال"] - 1]->{$data["گزینه"]}->{'محور'} == "t") {
            $answer->t = $answer->t + 1;
        }
        if ($answer->update() === false) {
            $this->response->setStatusCode(406);
            $this->response->setContent("we cant create answer");
            return $this->response;
        } elseif ($data["سوال"] == 59) {
            $this->response->setContent("finish");
            return $this->response;
        } else {
            $this->response->setContent("go on");
            return $this->response;
        }
    }

    public function testResult()
    {
        $answer = Answer::findFirst();
        if(!$answer){
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any result!");
            return $this->response;
        }else{
            $this->response->setJsonContent(
                $answer->toArray()
            );
            return $this->response;
        }
    }
}