<?php
namespace MyApp\Api\ServiceController;
use MyApp\Models\Services\Services;
use Phalcon\Di\Injectable;

class Controller extends Injectable
{
    public function getServices()
    {
        $services = Services::find();
        if(!$services){
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any service!");
            return $this->response; 
        }else{
            $this->response->setJsonContent(
                $services->toArray()
        );
        return $this->response;
        }
    }
    public function getService($id)
    {
        $service = Services::findFirst($id);
        if(!$service){
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any service with this id!");
            return $this->response;
        }else{
        $this->response->setJsonContent($service);
        return $this->response;
        }
    }
    public function createService()
    {
        $data = $this->request->getJsonRawBody(true);
        $service = new Services();
        $service->title = $data["title"];
        $service->description = $data["description"];
        $service->order = $data["order"];
        $service->price = $data["price"];
        if ($service->create() === false) {
            $this->response->setStatusCode(404);
            $this->response->setContent("we cant create service");
            return $this->response;
        } else {
            $this->response->setContent("Service created successfully");
            return $this->response;
        }
    }
    public function updateService($id)
    {
        $data = $this->request->getJsonRawBody(true);
        $Service = Services::findFirst($id);
        $Service->title = $data["title"];
        $Service->description = $data["description"];
        $Service->order = $data["order"];
        $Service->price = $data["price"];
        if ($Service->update() === false) {
            $this->response->setStatusCode(404);
            $this->response->setContent("we cant update Service");
            return $this->response;
        } else {
            $this->response->setContent("Service updated successfully");
            return $this->response;
        }
    }
    public function deleteService($id)
    {
        $service = Services::findFirst($id);
        if(!$service){
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any Service with this id!");
            return $this->response;
        }else{
            $service -> delete();
            $this->response->setContent("Service deleted successfully");
            return $this->response;
        }
    }
}
