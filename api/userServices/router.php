<?php

use Phalcon\Mvc\Micro\Collection as MicroCollection;

return call_user_func(function () {
    $userServices = new MicroCollection();
    $userServices->setHandler(new \MyApp\Api\userServicesController\Controller());
    $userServices->setPrefix('/userServices');
    $userServices->get('/', 'getUserServices');
    $userServices->get('/{id}', 'getUserService');
    $userServices->post('/', 'createUserServices');
    $userServices->put('/{id}', 'updateUserServices');
    $userServices->delete('/{id}', 'deleteUserServices');
    return $userServices;
});