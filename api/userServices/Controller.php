<?php

namespace MyApp\Api\userServicesController;

use MyApp\Models\UserServices\Userservices;
use Phalcon\Di\Injectable;

class Controller extends Injectable
{
    public function getUserServices()
    {
        $userServices = Userservices::find();
        if (!$userServices) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any userServices!");
            return $this->response;
        } else {
            $this->response->setJsonContent(
                $userServices->toArray()
            );
            return $this->response;
        }
    }

    public function getUserService($id)
    {
        $userService = Userservices::findFirst($id);
        if (!$userService) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any userService with this id!");
            return $this->response;
        } else {
            $this->response->setJsonContent($userService);
            return $this->response;
        }
    }

    public function createUserServices()
    {
        $data = $this->request->getJsonRawBody(true);
        $userService = new Userservices();
        $userService->user_id = $data["user_id"];
        $userService->service_id = $data["service_id"];
        $userService->status = $data["status"];
        $userService->start = $data["start"];
        $userService->finish = $data["finish"];
        if ($userService->create() === false) {
            $this->response->setStatusCode(404);
            $this->response->setContent("we cant create userService");
            return $this->response;
        } else {
            $this->response->setContent("UserService created successfully");
            return $this->response;
        }
    }

    public function updateUserServices($id)
    {
        $data = $this->request->getJsonRawBody(true);
        $userService = Userservices::findFirst($id);
        $userService->user_id = $data["user_id"];
        $userService->service_id = $data["service_id"];
        $userService->status = $data["status"];
        $userService->start = $data["start"];
        $userService->finish = $data["finish"];
        if ($userService->update() === false) {
            $this->response->setStatusCode(404);
            $this->response->setContent("we cant update UserService");
            return $this->response;
        } else {
            $this->response->setContent("UserService updated successfully");
            return $this->response;
        }
    }

    public function deleteUserServices($id)
    {

        $userService = Userservices::findFirst($id);
        if (!$userService) {
            $this->response->setStatusCode(404);
            $this->response->setContent("Oops.. we cant find any UserService with this id!");
            return $this->response;
        } else {
            $userService->delete();
            $this->response->setContent("UserService deleted successfully");
            return $this->response;
        }
    }
}
