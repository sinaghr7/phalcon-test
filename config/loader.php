<?php

use Phalcon\Loader;

$loader = new Loader();
$loader->registerNamespaces(
    [
        'MyApp\Models\Users' => '../models/',
        'MyApp\Models\Services' => '../models/',
        'MyApp\Models\UserServices' => '../models/',
        'MyApp\Models\Test' => '../models/',
        'MyApp\Models\Answer' => '../models/',
        'MyApp\Api\UserController' => '../api/users',
        'MyApp\Api\ServiceController' => '../api/services',
        'MyApp\Api\userServicesController' => '../api/userServices',
        'MyApp\Api\TestsController' => '../api/tests'
    ]
);
$loader->registerFiles(
    [
        '../middlewares/Auth.php',
        '../middlewares/acl.php'
    ]
);
$loader->register();