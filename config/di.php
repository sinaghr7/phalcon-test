<?php
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;

//database
$di = new FactoryDefault();
$di->set(
    'db',
    function () {
        return new PdoMysql(
            [
                'host'     => 'localhost',
                'username' => 'root',
                'password' => 'sinaghr7',
                'dbname'   => 'phalcon',
            ]
        );
    }
);