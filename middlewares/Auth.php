<?php

namespace MyApp\Middleware\Auth;

use Phalcon\Mvc\Micro;
use Phalcon\Crypt;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use MyApp\Models\Users\Users;
use Phalcon\Di\Injectable;

class AuthMiddleware extends Injectable implements MiddlewareInterface
{
    public function call(Micro $app)
    {
        $x = $this->validateToken();
        if (!$x) {
            $app->response->setStatusCode(406);
            $app->response->setContent("unvalid token")->send();
            return $app->stop();
        }
            $app->getDI()->set('userRole', function () use ($x) {
                return $x;
            });
            return $app->getDI()->get("userRole");
    }

    public function validateToken()
    {
        $token = $this->request->getHeader("token");
        if ($token == null) {
            return "Guests";
        }
        $key = "secret";
        $crypt = new Crypt();
        try {
            $userName = $crypt->decryptBase64($token, $key);
            $userName = json_decode($userName);
            if ($userName == null) {
                return false;
            }
            $userName = $userName->userName;
        } catch (\Exeption $e) {
            $e->setStatusCode(406);
            $e->getMessage();
            return $e;
        }
        $user = Users::findFirst([
            "conditions" => "userName = :userName:",
            "bind" => [
                "userName" => $userName
            ],
        ]);
        $this->di->set('userObject', function () use ($user) {
            return $user;
        });
        if ($user) {
            return $user->role;
        } else {
            return false;
        }
    }
}