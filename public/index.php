<?php
use Swoole\Http\Server;
use Swoole\Http\Request;
use Swoole\Http\Response;
use MyApp\Middleware\Acl\AclPlugin;
use MyApp\Middleware\acl\UserRole;
use MyApp\Middleware\Auth\AuthMiddleware;
use Phalcon\Mvc\Micro;

try {
    //loader
    require_once('../config/loader.php');
    //database
    require_once('../config/di.php');
    // Users handler
    $user = require_once('../api/users/router.php');
    $service = require_once('../api/services/router.php');
    $userServices = require_once('../api/userServices/router.php');
    $tests = require_once('../api/tests/router.php');
    $auth = new AuthMiddleware();
    $acl = new AclPlugin();
    $app = new Micro();
    $app->setDI($di);
    $app->mount($user);
    $app->mount($service);
    $app->mount($userServices);
    $app->mount($tests);
    $app->before($auth);
    $app->before($acl);
    $app->notFound(function () use ($app) {
        var_dump("Not Found");
        $app->stop();
    });

    $http = new Server('127.0.0.1', 9501);

    $http->on("start", function (Server $server) {
        echo "Swoole http server is started at http://127.0.0.1:9501\n";
    });
    $http->on('WorkerStart', function($server, $workerId) use($di, $app) {
        $server->application = $app;
    });
    $http->on('request', function ($request, $response) use($di, $app) {

        $html = $app->handle($request->server['request_uri'])->getContent();
        $response->end($html);
    });
    $http->start();

//    $app->handle();
} catch (\Exception $e) {
    var_dump($e);
}
